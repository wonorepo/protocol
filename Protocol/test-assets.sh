#!/bin/bash

EMPTYHASH="QmNLei78zWmzUdbeRB3CiUfAizWUrbeeZh5K1rhAQKCh51"

if /bin/false
then
  1;
fi

PARTYA=$(truffle exec scripts/userStorage-addUser.js 0 QmY84T5Evkgn3qC1sAhcAqg8S6ubXiqMvSHa1khoPyR9ZR | tail -n 1)
PARTYB=$(truffle exec scripts/userStorage-addUser.js 1 QmNVZy3SpgRQSaDfXnhHsM9G2weYEEjhVvetLFKPB6SC4n | tail -n 1)
truffle exec scripts/token-give.js 1 10000
ASSET=$(truffle exec scripts/assetFactory-create.js QmW12jK64ntzChmSCoQwtShxF8WRo5BmXk5bd8J7Q13CMF 10 0 | tail -n 1)
echo Asset: $ASSET
START=$(date +%s --date='next hour')
END=$(date +%s --date='next day')
DEAL=$(truffle exec scripts/dealFactory-create.js 0 $ASSET 1 $EMPTYHASH $START $END | tail -n 1)
echo Deal: $DEAL
truffle exec scripts/deal-sign.js $DEAL 1
truffle exec scripts/token-transfer.js $DEAL 20 1 
truffle exec scripts/token-balanceOf.js $DEAL | tail -n 1
truffle exec scripts/deal-sign.js $DEAL 1

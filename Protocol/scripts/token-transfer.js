module.exports = async function(callback) {
  try {
    let from;
    if (process.argv[6] != null && process.argv[6].match(/^0x[0-9A-Fa-f]{40}$/))
      from = process.argv[6];
    else if (!isNaN(parseInt(process.argv[6], 10)))
      from = web3.eth.accounts[parseInt(process.argv[6], 10)];

    let amount;
    if (process.argv[5] != null && !isNaN(parseFloat(process.argv[5], 10)))
      amount = parseFloat(process.argv[5], 10);
    else
      throw 'Bad or missing amount';

    let to;
    if (process.argv[4] != null && process.argv[4].match(/^0x[0-9A-Fa-f]{40}$/))
      to = process.argv[4];
    else if (!isNaN(parseInt(process.argv[4], 10)))
      to = web3.eth.accounts[parseInt(process.argv[4], 10)];
    else
      throw 'Bad or missing to';
    
    const Token = artifacts.require("Token");
    const token = await Token.deployed();
    
    let opts = {};
    if (from)
      opts['from'] = from;
      
    let result = await token.transfer(to, web3.toWei(amount), opts).then((result) => {return result});
    console.log(result.receipt.status);
    callback();
  }
  catch(e) {
    callback(e);
  }
}

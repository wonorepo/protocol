const bs58 = require('bs58');

module.exports = async function(callback) {
  try {
    function toBytes32(ipfs) {
      return '0x' + bs58.decode(ipfs).toString('hex').substr(4);
    }
    
    let end;
    if (process.argv[9] != null && !isNaN(parseInt(process.argv[9], 10)))
      end = parseInt(process.argv[9], 10);
    else
      throw 'Bad or missing end';
    
    let start;
    if (process.argv[8] != null && !isNaN(parseInt(process.argv[8], 10)))
      start = parseInt(process.argv[8], 10);
    else
      throw 'Bad or missing start';
    
    let infoHash;
    if (process.argv[7] != null && process.argv[7].match(/^Qm.{44}$/))
      infoHash = toBytes32(process.argv[7]);
    else
      throw 'Bad or missing infoHash';
    
    let partyB;
    if (process.argv[6] != null && process.argv[6].match(/^0x[0-9A-Fa-f]{40}$/))
      partyB = process.argv[6];
    else if (!isNaN(parseInt(process.argv[6], 10)))
      partyB = web3.eth.accounts[parseInt(process.argv[6], 10)];
    else
      throw 'Bad or missing partyB';
    
    let partyA;
    if (process.argv[5] != null && process.argv[5].match(/^0x[0-9A-Fa-f]{40}$/))
      partyA = process.argv[5];
    else
      throw 'Bad or missing partyA';
    
    let type;
    if (process.argv[4] != null && process.argv[4].match(/^[01]$/))
      type = process.argv[4];
    else
      throw 'Bad or missing type';
    
    const DealFactory = artifacts.require("DealFactory");
    const dealFactory = await DealFactory.deployed();
    
    console.log(`dealFactory.create(${type}, ${partyA}, ${partyB}, ${infoHash}, ${start}, ${end})`);
    
    let deal = await dealFactory.create(type, partyA, partyB, infoHash, start, end).then((result) => {return `0x${result.receipt.logs[0].topics[1].substr(-40)}`});
    console.log(deal);
    callback();
  }
  catch(e) {
    callback(e);
  }
}

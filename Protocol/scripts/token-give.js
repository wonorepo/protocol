module.exports = async function(callback) {
  try {
    if (isNaN(parseFloat(process.argv[5], 10)))
      throw 'Bad or missing amount';
    let address;
    if (process.argv[4] != null && process.argv[4].match(/^0x[0-9A-Fa-f]{40}$/))
      address = process.argv[4];
    else if (!isNaN(parseInt(process.argv[4], 10)))
      address = web3.eth.accounts[parseInt(process.argv[4], 10)];
    else
      throw 'Bad or missing to';
    const Token = artifacts.require("Token");
    const token = await Token.deployed();
    let result = await token.give(address, web3.toWei(parseFloat(process.argv[5], 10))).then((result) => {return result});
    console.log(result.receipt.status);
    callback();
  }
  catch(e) {
    callback(e);
  }
}

const bs58 = require('bs58');

module.exports = async function(callback) {
  try {
    function toBytes32(ipfs) {
      return '0x' + bs58.decode(ipfs).toString('hex').substr(4);
    }

    let infoHash;
    if (process.argv[5] != null && process.argv[5].match(/^Qm.{44}$/))
      infoHash = toBytes32(process.argv[5]);
    else
      throw 'Bad or missing infoHash';
    
    let address;
    if (process.argv[4] != null && process.argv[4].match(/^0x[0-9A-Fa-f]{40}$/))
      address = process.argv[4];
    else if (!isNaN(parseInt(process.argv[4], 10)))
      address = web3.eth.accounts[parseInt(process.argv[4], 10)];
    else
      throw 'Bad or missing address';

    const UserStorage = artifacts.require("UserStorage");
    const userStorage = await UserStorage.deployed();
    
    address = await userStorage.addUser(address, infoHash).then((result) => {return `0x${result.receipt.logs[0].topics[1].substr(-40)}`});
    console.log(address);
    callback();
  }
  catch(e) {
    callback(e);
  }
}



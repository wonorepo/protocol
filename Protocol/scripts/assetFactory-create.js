const bs58 = require('bs58');

module.exports = async function(callback) {
  try {
    function toBytes32(ipfs) {
      return '0x' + bs58.decode(ipfs).toString('hex').substr(4);
    }
    
    let unit;
    if (process.argv[6] != null && !isNaN(parseInt(process.argv[6], 10)))
      unit = parseInt(process.argv[6], 10);
    else
      throw 'Bad or missing unit';
    
    let price;
    if (process.argv[5] != null && !isNaN(parseFloat(process.argv[5])))
      price = parseFloat(process.argv[5]);
    else
      throw 'Bad or missing price';
    
    let infoHash;
    if (process.argv[4] != null && process.argv[4].match(/^Qm.{44}$/))
      infoHash = toBytes32(process.argv[4]);
    else
      throw 'Bad or missing infoHash';
    
    const AssetFactory = artifacts.require("AssetFactory");
    const assetFactory = await AssetFactory.deployed();
    
    let asset = await assetFactory.create(infoHash, web3.toWei(price), unit).then((result) => {return `0x${result.receipt.logs[0].topics[1].substr(-40)}`});
    console.log(asset);
    callback();
  }
  catch(e) {
    callback(e);
  }
}


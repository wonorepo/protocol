module.exports = async function(callback) {
  try {
    console.log(process.argv);

    let from;
    if (process.argv[5] != null && process.argv[5].match(/^0x[0-9A-Fa-f]{40}$/))
      from = process.argv[5];
    else if (!isNaN(parseInt(process.argv[5], 10)))
      from = web3.eth.accounts[parseInt(process.argv[5], 10)];
    else
      throw 'Bad or missing from';

    let dealAddress;
    if (process.argv[4] != null && process.argv[4].match(/^0x[0-9A-Fa-f]{40}$/))
      dealAddress = process.argv[4];
    else
      throw 'Bad or missing dealAddress';
    
    const SimpleDeal = artifacts.require("SimpleDeal");
    const deal = await SimpleDeal.at(dealAddress);
    
    let opts = {};
    if (from)
      opts['from'] = from;
      
    let result = await deal.confirm(opts).then((result) => {return result});
    console.log(result.receipt.status);
    callback();
  }
  catch(e) {
    callback(e);
  }
}

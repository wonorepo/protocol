var UserStorage = artifacts.require("./UserStorage.sol");

module.exports = function(deployer, network, accounts) {
    deployer
    .deploy(UserStorage)
    .catch((e) => {
        console.error(`\x1b[31;1m${e}\x1b[0m`);
    });
};

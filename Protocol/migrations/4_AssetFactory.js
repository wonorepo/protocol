var AssetFactory = artifacts.require("./AssetFactory.sol");

module.exports = function(deployer, network, accounts) {
    deployer
    .deploy(AssetFactory)
    .catch((e) => {
        console.error(`\x1b[31;1m${e}\x1b[0m`);
    });
};

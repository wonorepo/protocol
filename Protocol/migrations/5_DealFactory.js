var DealFactory = artifacts.require("./DealFactory.sol");
var Token = artifacts.require("./Token.sol");

module.exports = function(deployer, network, accounts) {
    deployer
    .deploy(DealFactory, Token.address)
    .catch((e) => {
        console.error(`\x1b[31;1m${e}\x1b[0m`);
    });
};

pragma solidity ^0.4.23;

import "openzeppelin-solidity/contracts/lifecycle/Destructible.sol";
import "openzeppelin-solidity/contracts/ownership/HasNoTokens.sol";
import "openzeppelin-solidity/contracts/ownership/HasNoEther.sol";

contract UserStorage is Ownable, HasNoTokens, HasNoEther {
    mapping(address => bytes32) public users;

    event userAdded(address indexed _address, bytes32 _infoHash);
    event userUpdated(address indexed _address, bytes32 _infoHash);
    event userDeleted(address indexed _address);

    constructor() public {}

    function addUser(address _address, bytes32 _infoHash) public {
        users[_address] = _infoHash;
        emit userAdded(_address, _infoHash);
    }

    function updateUser(bytes32 _infoHash) public {
        require(users[msg.sender] != 0x00);
        users[msg.sender] = _infoHash;
        emit userUpdated(msg.sender, _infoHash);
    }

    function deleteUser() public {

        // TODO delete all user's assets

        delete users[msg.sender];
        emit userDeleted(msg.sender);
    }

}
pragma solidity ^0.4.23;

import "./EntityFactory.sol";
import "./Asset.sol";
import "./lib/Versioned.sol";

contract AssetFactory is EntityFactory, Versioned(0x00000007) {
    constructor () public { }

    event assetCreated(address indexed _address, bytes32 _infoHash, uint _price, Asset.EUnit _unit, bytes4 _version);
    
    function create(bytes32 _infoHash, uint _price, Asset.EUnit _unit) public returns(address) {
        Asset asset = new Asset(_infoHash, _price, _unit, address(this));
        emit assetCreated(address(asset), _infoHash, _price, _unit, version);
        asset.transferOwnership(msg.sender);
        return address(asset);
    }
}

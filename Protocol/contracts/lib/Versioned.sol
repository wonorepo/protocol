pragma solidity ^0.4.23;

contract Versioned {
    bytes4 public version;

    constructor(bytes4 _version) public {
        version = _version;
    }
}

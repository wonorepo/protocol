pragma solidity ^0.4.23;

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/lifecycle/TokenDestructible.sol";

contract Entity is Ownable, TokenDestructible {

    bytes32 public infoHash;
    address public factory;

    constructor(bytes32 _infoHash, address _factory) internal {
        infoHash = _infoHash;
        factory = _factory;
    }

    function update(bytes32 _infoHash) external;
}

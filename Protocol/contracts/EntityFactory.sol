pragma solidity ^0.4.23;

import "openzeppelin-solidity/contracts/lifecycle/Destructible.sol";
import "openzeppelin-solidity/contracts/ownership/HasNoTokens.sol";
import "openzeppelin-solidity/contracts/ownership/HasNoEther.sol";

contract EntityFactory is Ownable, Destructible, HasNoTokens, HasNoEther {
    constructor() public {}
}

pragma solidity ^0.4.23;

import "./EntityFactory.sol";
import "./lib/Versioned.sol";
import "./SimpleDeal.sol";
import "./FreelanceDeal.sol";
import "openzeppelin-solidity/contracts/token/ERC20/DetailedERC20.sol";

contract DealFactory is EntityFactory, Versioned(0x0000000A) {
    DetailedERC20 public token;

    constructor (address _token) public {
        token = DetailedERC20(_token);
    }
    
    event dealCreated(address indexed _address, Deal.DealType _type, address indexed _partyA, address indexed _partyB, bytes32 _infoHash, uint32 _start, uint32 _end, bytes4 _version);

    function create(Deal.DealType _type, address _partyA, address _partyB, bytes32 _infoHash, uint32 _start, uint32 _end) public returns(address) {
        Deal deal;
        if (_type == Deal.DealType.Simple) {
            deal = new SimpleDeal(_partyA, _partyB, _infoHash, _start, _end, address(this));
            emit dealCreated(address(deal), _type, _partyA, _partyB, _infoHash, _start, _end, version);
        }
        else if (_type == Deal.DealType.Freelance) {
            deal = new FreelanceDeal(_partyA, _partyB, _infoHash, _start, _end, address(this));
            emit dealCreated(address(deal), _type, _partyA, _partyB, _infoHash, _start, _end, version);
        }
        else
            revert("Unsupported deal type");
        deal.setToken(address(token));
        return address(deal);
    }
}

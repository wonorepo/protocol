pragma solidity ^0.4.23;

import "./Entity.sol";
import "./AssetFactory.sol";

contract Asset is Entity {
    enum EUnit {
        Day,
        Month,
        Year,
        Lot
    }
    
    bool public inUse;
    uint public price;
    EUnit public unit;

    constructor(bytes32 _infoHash, uint _price, EUnit _unit, address _factory) public Entity(_infoHash, _factory) {
        inUse = false;
        price = _price;
        unit = _unit;
        //emit assetCreated(address(this), _infoHash, _price, _unit, AssetFactory(_factory).version());
    }
    
    modifier notInUse() {
        require(!inUse);
        _;
    }

    //event assetCreated(address indexed _address, bytes32 _infoHash, uint _price, EUnit _unit, bytes4 _version);
    event assetUpdated(address indexed _address, bytes32 _infoHash);
    event assetDeleted(address indexed _address);
    event assetPriceUpdated(address indexed _address, uint _price);

    function update(bytes32 _infoHash) external onlyOwner() notInUse() {
        infoHash = _infoHash;
        emit assetUpdated(address(this), infoHash);
    }
    
    function remove() external onlyOwner() notInUse() {
        emit assetDeleted(address(this));
        selfdestruct(msg.sender);
    }
    
    function isAsset() external pure returns (bool) {
        return true;
    }
    
    function updatePrice(uint _price) external {
        price = _price;
        emit assetPriceUpdated(address(this), price);
    }
}

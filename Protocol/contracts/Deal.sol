pragma solidity ^0.4.23;

import "./Entity.sol";
import "./Asset.sol";
import "openzeppelin-solidity/contracts/token/ERC20/DetailedERC20.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";
        
contract Deal is Entity {
    using SafeMath for uint;

    address public partyA;
    address public partyB;
    bool isAssetA;
    bool isAssetB;
    DetailedERC20 public token;
    bool public signedByA;
    bool public signedByB;
    int8 public confirmedByA;
    int8 public confirmedByB;
    bool public redeemed;
    uint32 public start;
    uint32 public end;

    enum DealType {
        Simple,
        Freelance
    }
    
    constructor (address _partyA, address _partyB, bytes32 _infoHash, uint32 _start, uint32 _end, address _factory) internal Entity(_infoHash, _factory) {
        require(_end >= _start);
        partyA = _partyA;
        if (isContract(_partyA))
            isAssetA = Asset(_partyA).isAsset();

        partyB = _partyB;
        if (isContract(_partyB))
            isAssetB = Asset(_partyB).isAsset();

        redeemed = false;
        start = _start;
        end = _end;
    }
    
    //event dealCreated(address indexed _address, address indexed _partyA, address indexed _partyB, bytes32 _infoHash, uint32 _start, uint32 _end, bytes4 _version);
    event dealSignedByA(address indexed _address);
    event dealSignedByB(address indexed _address);
    event dealSigned(address indexed _address);
    event dealConfirmed(address indexed _address);
    event dealConfirmedByA(address indexed _address);
    event dealConfirmedByB(address indexed _address);
    event dealDisputed(address indexed _address);
    event dealDisputedByA(address indexed _address);
    event dealDisputedByB(address indexed _address);
    event dealCanceled(address indexed _address);
    event dealRedeemed(address indexed _address, address _payee, uint _amount);
    event dealCancelledByA(address indexed _address);
    event dealCancelledByB(address indexed _address);

    function setToken(address _addr) public {
        require(address(token) == 0);
        token = DetailedERC20(address(_addr));
    }
    
    function isContract(address _addr) internal view returns (bool){
        uint32 size;
        assembly {
            size := extcodesize(_addr)
        }
        return (size > 0);
    }
    
    function update(bytes32 /* _infoHash */) external {
        revert("Deal can not be updated");
    }
    
    function sign() public {
        require(address(token) != 0);
        require(block.timestamp < start);
        if (msg.sender == partyA) {
            require(!signedByA);
            signedByA = true;
            emit dealSignedByA(address(this));
        }
        else if (msg.sender == partyB) {
            require(!signedByB);
            signedByB = true;
            emit dealSignedByB(address(this));
        }
        else
            revert();
        if (signedByA && signedByB)
            emit dealSigned(address(this));
    }
        
    function confirm() public {
        if (msg.sender == partyA) {
            require(confirmedByA == 0);
            confirmedByA = 1;
            emit dealConfirmedByA(address(this));
        }
        else if (msg.sender == partyB) {
            require(confirmedByB == 0);
            confirmedByB = 1;
            emit dealConfirmedByB(address(this));
        }
        else
            revert();
            
        if (isConfirmed())
            emit dealConfirmed(address(this));
    }
    
    function dispute() public {
        if (msg.sender == partyA) {
            require(confirmedByA == 0);
            confirmedByA = -1;
            emit dealDisputedByA(address(this));
        }
        else if (msg.sender == partyB) {
            require(confirmedByB == 0);
            confirmedByB = -1;
            emit dealDisputedByB(address(this));
        }
        else
            revert();
            
        if (isDisputed())
            emit dealDisputed(address(this));
    }
    
    function cancel() public {
        if (msg.sender == partyA) {
            emit dealCancelledByA(address(this));
            selfdestruct(msg.sender);
        }
        else if (msg.sender == partyB) {
            emit dealCancelledByB(address(this));
            selfdestruct(msg.sender);
        }
        else
            revert();
    }
    
    function isSigned() public view returns (bool) {
        return signedByA && signedByB;
    }
    
    function isConfirmed() public view returns (bool) {
        return confirmedByA > 0 && confirmedByB > 0;
    }
    
    function isDisputed() public view returns (bool) {
        return confirmedByA < 0 || confirmedByB < 0;
    }
    
    function isStarted() public view returns (bool) {
        return block.timestamp >= start;
    }
    
    function isEnded() public view returns (bool) {
        return block.timestamp > end;
    }
}

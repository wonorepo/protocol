pragma solidity ^0.4.23;

import "./Asset.sol";
import "./Deal.sol";
import "./DealFactory.sol";

contract FreelanceDeal is Deal {
    DealType public dealType;
    Asset public asset;
    uint8 public brokeragePercent;
    uint256 public milestones;
    uint256 public milestonesCompleted;

    constructor(address _partyA, address _partyB, bytes32 _infoHash, uint32 _start, uint32 _end, address _factory) public
        Deal(_partyA, _partyB, _infoHash, _start, _end, _factory)
    {
        require( isAssetA);
        require(!isAssetB);

        dealType = DealType.Freelance;
        asset = Asset(address(_partyA));
        signedByA = true;
        brokeragePercent = 1;
        milestones = 1;

        //emit dealCreated(address(this), partyA, partyB, infoHash, start, end, DealFactory(_factory).version());
        emit dealSignedByA(address(this)); 
    }
    
    function () external payable {
        revert();
    }
    
    function isPayed() public view returns (bool) {
        return token.balanceOf(address(this)) >= cost().add(brokerage());
    }
    
    function change() public {
        uint changeAmount = token.balanceOf(address(this)).sub(brokerage());
        if (!redeemed)
            changeAmount = changeAmount.sub(cost());

        token.transfer(partyB, changeAmount);
    }

    function sign() public {
        if (msg.sender == asset.owner()) {
            require(!signedByA);
            signedByA = true;
            emit dealSignedByA(address(this));
        }
        else if (msg.sender == partyB) {
            require(!signedByB);
            require(this.isPayed());
            signedByB = true;
            emit dealSignedByB(address(this));
        }
        else
            revert();
        if (signedByA && signedByB)
            emit dealSigned(address(this));
    }
    
    function confirm() public {
        if (msg.sender == asset.owner()) {
            require(confirmedByA == 0);
            confirmedByA = 1;
            emit dealConfirmedByA(address(this));
        }
        else
            super.confirm();
            
        if (isConfirmed()) {
            token.transfer(asset.owner(), cost());
            //token.transfer(treasury, brokerage());
            redeemed = true;
            emit dealRedeemed(address(this), asset.owner(), cost());
        }
    }
    
    function dispute() public {
        if (msg.sender == asset.owner()) {
            require(confirmedByA == 0);
            confirmedByA = 1;
            emit dealDisputedByA(address(this));
            token.transfer(asset.owner(), cost());
            redeemed = true;
            emit dealRedeemed(address(this), asset.owner(), cost());
        }
        else {
            super.dispute();
            token.transfer(partyB, cost());
            redeemed = true;
            emit dealRedeemed(address(this), partyB, cost());
        }
    }
    
    function cancel() public {
        if (msg.sender == asset.owner()) {
            token.transfer(partyB, token.balanceOf(address(this)));
            emit dealCancelledByA(address(this));
            selfdestruct(msg.sender);
        }
        else if (msg.sender == partyB) {
            if (isSigned() && start - 86400 < block.timestamp)
                token.transfer(asset.owner(), cost());
            else
                token.transfer(partyB, token.balanceOf(address(this)));
            emit dealCancelledByB(address(this));
            selfdestruct(msg.sender);
        }
        else
            revert();
    }
    
    function quantity() public view returns (uint32) {
        return uint32(countMilestones());
    }
    
    function cost() public view returns (uint) {
        return asset.price().mul(quantity());
    }
    
    function brokerage() public view returns (uint) {
        return cost().mul(brokeragePercent).div(100);
    }

    function defineMilestones(uint8 n) public {
        require(milestones == 1);
        require(msg.sender == asset.owner());
        for (uint8 i = 0; i < n; ++i) {
              milestones = milestones | ( uint256(1) << i );
        }
    }

    function completeMilestone(uint8 n) public {
        require(msg.sender == partyB);
        require(milestones & (uint256(1) << n) == 1);
        require(milestonesCompleted & (uint256(1) << n) == 0);
        milestonesCompleted = milestonesCompleted | ( uint256(1) << n );
    }

    function allMilestonesCompleted() public view returns (bool) {
        return bool(milestones == milestonesCompleted);
    }
    
    function countMilestones() public view returns (uint8) {
        uint8 c = 0;
        uint256 m = milestones;
        for (uint8 i = 0; i < 256 && m > 0; ++i)
            if (m & (uint256(1) << i) == 1) {
                m = m ^ (uint256(1) << i);
                ++c;
            }
        return c;
    }
}
